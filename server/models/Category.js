const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    parent: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Category'
    }

})

//导出关联内容
schema.virtual('children',{
    localField: '_id',
    foreignField: 'parent',
    justOne: false,
    ref: 'Category'
})
schema.virtual('newsList',{
    localField: '_id',
    foreignField: 'Category',
    justOne: false,
    ref: 'Article'
})

module.exports = mongoose.model('Category', schema)