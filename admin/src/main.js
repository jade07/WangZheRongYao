import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import './plugins/element.js'

import './style.css'

Vue.config.productionTip = false

import http from './http'
Vue.prototype.$http = http

//图片处理
Vue.mixin({
  computed: {
    uploadUrl(){
      return this.$http.defaults.baseURL + '/upload'
    }
  },
  methods: {
    getAuthHeaders(){
      return {
        Authorization: `Bearer ${localStorage.token || ''}`
      }
    }
  }

})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
